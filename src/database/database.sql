DROP SCHEMA IF EXISTS penedo;
CREATE SCHEMA penedo CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE penedo;

CREATE TABLE users (
    name           VARCHAR(45) NOT NULL,
    price          FLOAT       NOT NULL,
    hitch          FLOAT       NOT NULL,
    interest       FLOAT       NOT NULL,
    months         INTEGER     NOT NULL,
    total_interest FLOAT       NOT NULL,
    PRIMARY KEY (name)
);
