<?php
require_once (__DIR__.'/Twig/Autoloader.php');
Twig_Autoloader::register();

/* Load the templates */
$loader = new Twig_Loader_Filesystem(__DIR__.'/../../assets/html');
$twig = new Twig_Environment($loader);
