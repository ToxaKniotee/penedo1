<?php
require_once __DIR__.'/../DatabaseHelper.php';
require_once __DIR__.'/User.php';

class UserHelper extends DatabaseHelper {
    protected $class_name = 'User';
    protected $id_name = 'user_id';
    protected $table_name = 'users';

    public function __construct() {
        parent::__construct(
            $this->table_name,
            $this->class_name,
            $this->id_name
        );
    }
}
