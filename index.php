<?php
require_once __DIR__.'/src/php/includes.php';
$params = [];

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $user = new User();
    $user->populate($_POST);

    $total = round($user->price * (1 - $user->hitch), 2);
    $interest_rate = $user->interest / 12;
    $payment = round($total * ($interest_rate + $interest_rate / (pow(1 +
            $interest_rate, $user->months) - 1)), 2);
    $user->total_interest = 0;
    $table = [];

    for ($i = 0; $i < $user->months; $i++) {
        $current_interest = round($total * $interest_rate, 2);
        $capital = round($payment - $current_interest, 2);
        $total = round($total - $capital, 2);

        $user->total_interest += $current_interest;

        $table []= [
            'interes' => $current_interest,
            'capital' => $capital,
            'total'   => $total
        ];
    }

    $helper = new UserHelper();
    if (!$helper->insert($user)) {
        $helper->update($user);
    }

    $params['data'] = [
        'payment' => $payment,
        'table' => $table
    ];
}

echo $twig->render('index.twig', $params);
